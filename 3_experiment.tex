\chapter{Experimental setup}
\label{ch:apparatus}
The experimental setup is built around the technical requirements for
optical lattice experiments on ultracold quantum gases. The goal is to
have a machine which allows for experiments with bosons and fermions with
tunable interactions in a quasiperiodic optical lattice. We use bosonic
\K{39} and fermionic \K{40} as they have a Feshbach resonance on their own
at achievable magnetic field strengths. Bosonic \Rb{} is used as
sympathetic coolant but can also be used for experiments with weakly
interacting systems and as mixture with potassium.

In the first section we will look at the main apparatus. We follow the
different cooling steps required to cool the three different species into
the ultracold regime. We will also take a look at the lattice setup for
the actual experiments.

In the second section we will have a quick look at the complex laser
system providing the different light frequencies to control and cool the
atoms. This covers the lasers required for cooling as well as the lasers
used for optical trapping and building the lattice.

A detailed discussion of the automated control system is the topic of the
next chapter.

\section{Main apparatus}
The main experimental apparatus is an \gls{UHV} system. The system can be
divided into four different zones which are connected via differential
pumping sections. A 3D rendering of the vacuum system including the
magnetic transport can be found in figure~\ref{fig:apparatus_topdown}
which depicts the different zones. An image of the vacuum system after
bakeout without any optics or magnetic field components can be seen in
figure~\ref{fig:apparatus}.

At the time of writing the vacuum chamber was baked and the central
components for the magnetic fields are in place. For rubidium all
\gls{MOT} stages are assembled and we are in the process of optimizing
loading rates and final temperatures. For potassium many preparation steps
are done, but parts of the optical setup are still missing. Preparations
for the magnetic transport are done, testing and optimising it will be the
next major steps.

\begin{figure}
    \centering
    \tikzsetnextfilename{apparatus_topdown}
    \begin{tikzpicture}[
            box/.style={
                draw,
                ultra thick,
                rounded corners,
            },
            every node/.style={
                font=\sffamily\Large,
            },
        ]
        \tikzpicturedependsonfile{\builddir{Apparatus1.png}}
        % including the pdf shows no image
        \node[include image] (image) at (0,0)
            {\includegraphics{\builddir{Apparatus1.png}}};
        \draw[box, blue] (5.5,0) 
            node[anchor=north west, align=right] {Rb 2D\,MOT}
            rectangle (8.7,3.8);
        \draw[box, blue] (5.5,10.8)
            node[anchor=south west, align=right] {K 2D\,MOT}
            rectangle (8.7,7);
        \draw[box, green] (6,6.8)
            rectangle (8.7,4)
            node[anchor=south west, align=left] {3D\,MOT};

        \draw[box, red] (12,4.25) rectangle (14,2.25)
            node[anchor=north east, align=right] {science cell};
    \end{tikzpicture}
    \caption{3D rendering of the apparatus in a top-down view. Atoms are
        pre-cooled in the two 2D\,MOTs and pushed into the central 3D-MOT.
        After MOT cooling the atoms are loaded into a magnetic trap and
        are moved to the science cell. There evaporative cooling is used
        to reach the final temperatures and the experiments take place.
        The extensions to the left and the top contain the ion and
        titanium sublimation pumps necessary to maintain the \gls{UHV}
        pressure.
    }
    \label{fig:apparatus_topdown}
\end{figure}

\begin{figure}
    \centering
    \tikzsetnextfilename{apparatus}
    \begin{tikzpicture}[
            every node/.style={
                node font={\sffamily\Large},
                anchor=west,
                align=left,
                general shadow={
                    shadow scale=1.25,
                },
            },
        ]
        \tikzpicturedependsonfile{\imagedir{apparatus.jpg}}
        \node[include image] at (0,0)
            {\includegraphics{\imagedir{apparatus.jpg}}};
        
        \node at (3,3) {\stroke{Rb 2D\,MOT}};
        \node at (6.5,7.5) {\stroke{K 2D\,MOT}};
        \node at (5,4) {\stroke{3D\,MOT}};
        \node at (10,1) {\stroke{Science cell}};
    \end{tikzpicture}
    \caption{Image of the vacuum system after the final bakeout without
        any mounts for the magnetic field components or optics. One can
        see which components were part of the high temperature bakeout by
        their colour difference. On the 2D\,\glspl{MOT} one can see the
        large windows used for the transverse cooling beams.
    }
    \label{fig:apparatus}
\end{figure}

\subsection{MOTs}
Our experimental setup uses two 2D\,\glspl{MOT} (or 2D+ MOTs, as they have an
additional axial cooling beam), with each 2D\,\gls{MOT} containing one
species of atoms. Using these extra \glspl{MOT} increases our loading rate
compared to single \gls{MOT} setups and will allow us to finish the
\gls{MOT} cooling stage in roughly \SI{1}{\s}.

We can achieve these loading rates by having a \enquote{high} pressure of
around \SI{1e-7}{\mbar} in the 2D\,\gls{MOT} chambers. Large windows allow
the use of large cooling beams creating a large capture volume. This in
combination with the high atom concentration results in a high capture
rate of our 2D\,\glspl{MOT}. The captured atoms get pre-cooled and pushed
into the 3D\,MOT through a differential pumping section. It maintains a
pressure difference between the \gls{MOT} chambers and lets us achieve
\SI{5e-11}{\mbar} inside the main \gls{MOT} chamber. The low pressure
reduces heating effects which become important at low temperatures.

The 2D\,MOTs are mirror images of each other. Each \gls{MOT} uses two
elliptical beams for transverse cooling. These beams have a diameter of
\SI{20 x 40}{\mm} (double-waist) and are retro-reflected onto the other side
of the \gls{MOT}. A large mica quarter wave plate is placed in front of the
mirror to adjust the polarisation of the retro-reflected beam.

Additionally a third cooling beam is sent along the axial direction of the
\gls{MOT} and retro-reflected on a mirror inside the vacuum. A polariser
is mounted in front of the mirror which is used to control the intensity
of the retro-reflected light. The mirror and the polariser have a small
hole through which the atoms can leave the 2D\,MOT into the 3D\,MOT. The
hole cuts the center out of the retro-reflected beam. This creates an area
in which the atoms get pushed in the direction of the 3D\,MOT. In order to
increase that effect we used an additional blue detuned push beam with
small diameter aligned to the center of the whole.

We use two pairs of coils along the transverse cooling directions to
create the required magnetic fields. At the time of writing the potassium
2D\,MOT is not completely set up. We focussed on testing everything with
\Rb{} as it is easier to set up and required to sympathetically cool \K{}
to degeneracy. 

For our main \gls{MOT} we use a classic \gls{MOT} setup with two coils in
the Anti-Helmholtz configuration along the vertical direction. Three pairs
of counter-propagating beams along all axes are used for cooling.

During the first cooling stage we have all \glspl{MOT} running to collect
pre-cooled atoms from the 2D\,MOTs. At the end of the loading stage we
switch off the magnetic field only keeping the optical molasses running
for a few milliseconds. This reduces the temperatures further before the
atoms get spin-polarised and loaded into a magnetic trap.

\subsection{Magnetic transport}
Our apparatus separates \gls{MOT} and experiment chamber by transporting
the atoms in a magnetic trap. The atoms are transported through another
differential pumping section and around a corner to achieve the lowest
possible pressures inside the experiment chamber. In this way, we have
obtained pressures of \SI{1e-11}{\mbar} in the experiment chamber. An
additional benefit of this separation is the increased space for optical
components around the experiment chamber.

In a magnetic trap the atoms are trapped in the minimum of the magnetic
field by using the Zeeman effect described in section~\ref{subsec:mot}.
The atoms are optically pumped in a state which is attracted by the
magnetic field minimum. In our apparatus we move this minimum by ramping
the current in a series of overlapping coils. By ramping down one coil
pair while ramping up the next coil pair the minimum moves smoothly
between the two coil pairs. Figure~\ref{fig:threecoils} shows a
demonstration of that process with three coil pairs.

\begin{figure}
    \centering
    \includegraphics{\builddir{threecoils.png}}
    \caption{Translation of the magnetic field minimum. In the beginning
        only the central coil pair is active (left). The minimum is moved
        first by ramping up the right coil pair while ramping down the
        central coil pair (centre). In the end only the right coil pair is
        powered up (right).
    }
    \label{fig:threecoils}
\end{figure}

We use 18 transport coil pairs in addition to the coil pairs for the
\gls{MOT} and Feshbach fields to transport the atoms over a distance of
roughly half a meter. During the transport at most 3 coil pairs are
running at the same time. We take advantage of this by having only four
\SI{100}{\A} power supplies for all coils, with an additional \SI{200}{\A}
supply for the main experiment coils. \Gls{MOSFET} based switching boxes
direct the currents to the currently active coil pair. During the
transport three power supplies are ramped to control the active coils
while the fourth is turned off and switched to the next coil pair.

All coils are mounted into two brass cooling blocks (one for either side)
to keep them in place, which can be seen in
figure~\ref{fig:transport_coils}. Cooling water is pumped through both
blocks to remove the heat generated by strong current inside the coils.

\begin{figure}
    \centering
    \includegraphics{\builddir{transportcoils.png}}
    \caption{Placement of the coils inside the cooling block. The upper
        cooling block is omitted for clarity. The horizontal push coil
        (left) is wound directly around a vacuum pipe.
    }
    \label{fig:transport_coils}
\end{figure}

\subsection{Experiment chamber}
The experiment chamber is a glass cell sticking out of the main vacuum
system to allow optical access from all directions. After the atoms have
been moved into the chamber they are cooled by forced evaporation. This is
done firstly in the magnetic trap using \gls{RF} until the atoms are cold
enough to be loaded into an optical dipole trap.

Once the atoms are cooled we can load them into the optical lattice to
perform experiments. It is planned to have an experimental setup with five
lattice beams in even angles. We expect that this will create a
quasi-periodic structure.

As there are no retro-reflecting mirrors to create standing waves the
phase of the beams is not fixed. Phase fluctuations will move the
position of the interference minima and maxima. This requires us to add a
locking circuit which corrects for phase noise on the individual lattice
beams. This locking circuit will measure the phase noise of the light
reflected back through the optical fibre by interfering it with the
incoming beam. \Glspl{AOM} will be used to compensate for the measured
fluctuations.

\section{Laser system}
In this section I will shortly describe the laser system which is required
to drive the atomic transitions. Planning and building this setup was a
team effort, with single components constructed by different persons. I
concentrated on the \Rb{} offset lock which I will describe and characterise
in more detail in this section. 

A large portion of the laser system is required to drive different
transitions in the atoms. The hyperfine structure of the species used and
the required laser frequencies are shown in figure~\ref{fig:hyperfine}.

\begin{figure}
    \centering
    \includegraphics[width=\textwidth]{images/line.pdf}
    \caption{Hyperfine structure of the D2 line of \Rb, \K{39} and \K{40}.}
    \label{fig:hyperfine}
\end{figure}

The hyperfine splitting for all species is low enough to use \glspl{AOM}
to shift to the correct frequencies. We use this extensively in our setup
by having two diode master lasers for rubidium and potassium. Their output
is split, shifted using \glspl{AOM} and amplified using \glspl{TA}. Both
master lasers are locked on a vapour cell using Doppler-free spectroscopy.

Two additional diode lasers are used. The first drives the repump
transition in \Rb. Repumping is necessary as atoms can spontaneously decay
into the $F = 1$ state where they are no longer subject to the cooling
light. The repump laser makes sure that atoms are pumped back into the
cooling transition. We use a separate laser as the frequency splitting
between the $F=1$ and $F=2$ hyperfine states can not be effectively
bridged using \glspl{AOM}. The second dedicated laser is a diode laser
used to create the potassium image beams. We use a separate laser to
achieve the wide tuning range which we require for imaging of \K{39} and
\K{40}.

The potassium setup is designed to allow for fast switching between \K{39}
and \K{40} by including an \emph{isotope switch}. The switch is built
using two \glspl{AOM} in double pass configuration which compensate for
the different hyperfine splittings in the $4^2S_{1/2}$ manifold. Switching
between the species is done by enabling or disabling the \glspl{AOM}.
No realignment is necessary as the double pass configuration ensures that
the outgoing beams do not shift when the \gls{AOM} is tuned. When
switching between isotopes the repump and cooling components switch roles,
which works well for \K{} as cooling and repump are comparable in
intensity. Lastly, all other \glspl{AOM} in the further path are shifted to
correct for the different splittings in the $4^2P_{3/2}$ manifold.

Several other lasers are required for the planned lattice experiments.
All of them are already in the lab and tested, but as they are part of
later experiment stages they are not yet in use. These are a
\SI{1064}{nm} laser to create the dipole trap and a diode laser with
\gls{TA} to plug the magnetic trap. A Ti:Sa laser is used to create
the lattice beams.

\subsection{Rubidium offset lock}
\label{sec:rb_offset_lock}
The rubidium offset lock is one of the few classical laser locks
in our experimental setup. It is supposed to lock the dedicated repump
laser relative to the rubidium master laser. The master laser itself is
locked onto a vapour cell.

The locking scheme we use for this purpose is a \emph{delay line
lock}\autocite{schuenemann1999}. This scheme uses the beat signal between
the two lasers and the propagation properties of that beat signal in a
coaxial wire. A phase shift proportional to the frequency is picked up
during propagation through the wire. To generate the error signal the beat
signal is split and recombined after one of the two connections is sent
through a piece of coaxial wire. This is the delay line which gives the
scheme its name. The mixing process generates the difference and the sum
frequency between both inputs. As both inputs are the same split
frequency, the difference frequency is a \gls{DC} signal only depending on
the phase difference introduced by the delay line. As this phase
difference is proportional to the beat frequency it can be used as an
error signal for a \gls{PID} controller after the sum frequency has been
filtered using a low pass.

\begin{figure}
    \tikzsetnextfilename{repumper_lock_scan}
    \begin{tikzpicture}[scope]
        \pgfplotstableread[col sep=comma]{data/Rb_repump_full_range.csv}\table
        \begin{axis}[
                auto measured axis,
                title=Scan of the repump lock signal,
                x SI prefix=milli,
                x unit=s,
                xlabel=piezo scan time,
                ylabel=error signal,
                y unit=V,
                y SI prefix=milli,
            ]
            \addplot table[y index=2] {\table};
            \draw[dashed] 
                (axis cs:0.08407,\pgfkeysvalueof{/pgfplots/ymax})
                -- (axis cs:0.08407,\pgfkeysvalueof{/pgfplots/ymin});
            \draw[<->, thick]
                (axis cs:0.0522,.9*\pgfkeysvalueof{/pgfplots/ymax})
                    -- (axis cs:0.08407,.9*\pgfkeysvalueof{/pgfplots/ymax})
                    node[midway,below] {\SI{6.7}{\GHz}};
        \end{axis}
    \end{tikzpicture}
    \caption{Error signal generated by the offset lock for a wide range
        scan of the repump laser. The center of the symmetry equals the
        locked master laser frequency. The dashed line denotes the
        repumper lock point. The scan time corresponds to a single scan of
        the piezo with an amplitude of approximately \SI{40}{Vpp}.
    }
    \label{fig:repumper_lock_scan}
\end{figure}

For our application we want the beat frequency to be at
\SI{6.7}{\GHz}\footnote{This frequency is a bit lower than the hyperfine
splitting in the $5^2S_{1/2}$ manifold which is \SI{6.834}{\GHz}. The
repump transition is from the $F=1$ to the $F'=2$ state while the master
laser is locked on the crossover signal between the $F'=2$ and $F'=3$
states.}. This high frequency is complicated to work with as losses in
wires increase significantly and specialised electrical components are
required. Our setup uses an additional mixing stage in front of the delay
line scheme to reduce the frequency to about \SI{800}{\MHz}. Connections
between all components up to the mixer were made as short as possible to
reduce losses.

To get a usable signal we use \SI{1}{\mW} of laser power per beam, which
is close to the damage threshold of the photo diode. We overlap both beams
using a 50:50 beam splitter and focus onto a fast photo diode (Hamamatsu
G4176-03). The photo diode is mounted directly in front of two
pre-amplifiers (Kuhne LNA BB0180 A-SMA), the first modified by the
manufacturer to include a Bias-T. To generate a frequency which is easier
to handle we mix (Mini-Circuits ZMX-10G+) the resulting frequency with a
\SI{7.5}{\GHz} reference (manufactured by Kuhne). The intermediate
frequency is sent through a \SI{3}{\dB} attenuator (Mini-Circuits VAT-3+)
and low pass (Mini-Circuits VLFX 1125) to the splitter (Mini-Circuits
ZFSC-2-11+). One output is sent directly into the mixer (Mini-Circuits
ZFM-4-S+), the other is sent through \SI{1}{\m} of coaxial wire. The
resulting error signal is filtered using a low pass (Mini-Circuits
SLP-1.9+) and sent to the \gls{PID} controller inside the laser
controller.

The laser controller uses two \gls{PID} controllers with different
reaction constants. One controller moves the grating which resembles one
end of the cavity a by controlling a piezo. It has a large tuning range
but the physical movement of the mirror limits the reaction time. The
second controller modulates the supply current of the laser diode. This
changes the refractive index and thus the laser frequency. This process is
faster, but can only tune the laser in a limited range. When the lock is
active both controllers work together combining the advantages of both
methods.

A full scan of the error signal is displayed in
figure~\ref{fig:repumper_lock_scan}. The plot shows a clear symmetry which
is expected as the beat signal is the absolute value of the difference
between the laser frequencies. As we are scanning the repump laser while
the master laser is locked the center of the symmetry can be identified as
the master laser frequency. The fringes next to the center are the error
signal generated by the beat frequency leaking through the first mixing
stage. Higher frequencies are dampened by the following low pass. In the
range of \SIrange{6}{9}{\GHz} the difference frequency between the beat
signal and the \SI{7.5}{\GHz} reference can pass through the low pass,
generating the wanted fringes.

We determined the lock point for the repump transition while optimising the
rubidium \gls{MOT} stages. Identifying this lock point in the plot allows
to convert the x axis to a frequency scale. Using this we calculated a
conversion factor from the amplitude of the error signal to the
corresponding frequency fluctuation. Comparison with a scan of the error
signal while the lock is active shows a lock stability in the order of
\SI{500}{\kHz}. We are currently waiting for a stabilised cavity which can
be used to precisely determine the lock stability. This will allow to give
a better indication of the lock stability as it measures the actual
fluctuations of the light instead of the resulting error signal which is
influenced by two stabilised lasers.

\subsection{Monitoring photodiode}
As a small side project we have created a simple photo diode design. We
wanted to have a cheap and easy to use photo diode design for
monitoring purposes throughout the experiment. Every output of a fibre has
as pick-off plate to one of these photo diodes.

Building a monitoring photo diode by itself is a trivial task, which only
requires to apply a reverse voltage onto a regular photo diode. Light
absorbed by the photo diode will create a current proportional to the
light intensity. The schematic can be seen in
figure~\ref{fig:photodiode_schematic}. The diode is a BPW34 and is
supplied from an external \SI{9}{\V} battery. The value of the resistance
is a trade-off between reaction speed of the photodiode and power
consumption. We chose the resistance for a bandwidth of \SI{1}{\MHz}.
Assuming a constant illumination with \SI{1}{\mW\per\square\centi\meter} a
usual \SI{9}{\V} block should last for about two years.

\begin{figure}
    \centering
    \begin{subfigure}[b]{0.4\textwidth}
        \centering
        \includegraphics[width=\textwidth]{build/round-photodiode.pdf}

        \vspace{.5cm}
        \includegraphics[width=.45\textwidth]{build/round-photodiode-brd.pdf}
        \vspace{.5cm}
        \caption{Schematic (top) and PCB layout (bottom) of the photo
        diode.}
        \label{fig:photodiode_schematic}
        \label{fig:photodiode_pcb}
    \end{subfigure}
    \begin{subfigure}[b]{0.45\textwidth}
        \centering
        \includegraphics{images/photodiode.jpg}
        \caption{Assembled photo diode}
        \label{fig:photodiode_photo}
    \end{subfigure}
    \caption{Simple photo diode}
\end{figure}

To mount them easily on the table we designed a round single layer
\gls{PCB} with \SI{1}{\inch} diameter which is shown in
figure~\ref{fig:photodiode_pcb}. This allows us to mount the photo diode
into standard optics components which we normally use to mount lenses.
These mounts have an internal thread which is compatible with our lens
tubes, filters and fibre adapters.

An assembled photo diode mounted into a lens mount is shown in
figure~\ref{fig:photodiode_photo}. The \glspl{PCB} were manufactured in
house by the electronics workshop. Instead of the usual silk layer we used
the copper layer to add annotations as the workshop lacks the capabilities
to add a silk layer.

% vim: spell spelllang=en
