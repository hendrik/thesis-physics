MAIN=thesis
TARGETS=$(MAIN).pdf

TMP_DIR = build
DEPS_DIR = $(TMP_DIR)
FIG_DIR = $(TMP_DIR)/figs
IMG_DIR = images
DATA_DIR = data

LATEXMK = latexmk -use-make -deps
PROPER_TEXFOT=env max_print_line=1000 error_line=254 half_error_line=238

all: $(TARGETS)

FIGS=$(shell cat $(TMP_DIR)/$(MAIN).figlist)

DEP_FILES=$(wildcard *.tex)
DEP_FILES+=$(wildcard *.bib)
DEP_FILES+=$(shell find images)
#DEP_FILES+=$(shell find data)

include $(FIGS:%=%.dep)

$(TMP_DIR)/%.pdf: %.tex $(DEP_FILES)
	@mkdir -p $(DEPS_DIR) $(TMP_DIR) $(FIG_DIR)
	@mkdir -p $(TMP_DIR)/$(FIG_DIR)
	@rm -f $@
	$(PROPER_TEXFOT) $(LATEXMK) -deps-out=$@.P -e '$$max_repeat=1;' $< || true
	$(MAKE) figures
	$(PROPER_TEXFOT) $(LATEXMK) -g -deps-out=$@.P $<

%.pdf: $(TMP_DIR)/%.pdf
	ln -f $(TMP_DIR)/$@ .
	git commit -a -m="autocommit" || true

figures: $(FIGS:%=%.pdf)

$(TMP_DIR)/%.png: $(IMG_DIR)/%.pdf
	@mkdir -p $(TMP_DIR)
	convert $^ -geometry 1700x -units PixelsPerInch -density 300 $@

$(TMP_DIR)/%.pdf: $(IMG_DIR)/%.svg
	@mkdir -p $(TMP_DIR)
	inkscape --export-text-to-path --export-area-drawing --export-pdf $@ $<

$(TMP_DIR)/%.png: $(IMG_DIR)/%.svg
	@mkdir -p $(TMP_DIR)
	convert $^ -geometry 1700x -units PixelsPerInch -density 300 $@

$(FIG_DIR)/apparatus.pdf: $(TMP_DIR)/$(FIG_DIR)/apparatus.md5
	@mkdir -p $(FIG_DIR)
	$(PROPER_TEXFOT) texfot xelatex -shell-escape -halt-on-error \
	    -jobname "$(subst .pdf,,$@)" \
	    "\def\tikzexternalrealjob{$(MAIN)}\input{$(MAIN)}"

$(FIG_DIR)/%.pdf: $(TMP_DIR)/$(FIG_DIR)/%.md5
	@mkdir -p $(FIG_DIR)
	$(PROPER_TEXFOT) texfot lualatex -shell-escape -halt-on-error \
	    -jobname "$(subst .pdf,,$@)" \
	    "\def\tikzexternalrealjob{$(MAIN)}\input{$(MAIN)}"

$(FIG_DIR)/%.dep:
	@mkdir -p $(FIG_DIR)
	@touch $@

# tikz dependencies are broken and just get written once the file is
# actually created
$(FIG_DIR)/apparatus_topdown.pdf: $(TMP_DIR)/Apparatus1.png
$(TMP_DIR)/$(MAIN).pdf: $(TMP_DIR)/University_of_Cambridge_logo.pdf $(TMP_DIR)/LMU_Muenchen_logo.pdf
$(TMP_DIR)/$(MAIN).pdf: $(TMP_DIR)/threecoils.png $(TMP_DIR)/transportcoils.png
$(TMP_DIR)/$(MAIN).pdf: $(TMP_DIR)/round-photodiode-brd.pdf $(TMP_DIR)/round-photodiode.pdf
$(TMP_DIR)/$(MAIN).pdf: $(TMP_DIR)/bandstructure.pdf

clean:
	rm -rf $(TARGETS) $(TMP_DIR)

.PHONY: all clean figures
