$pdflatex = 'texfot xelatex -synctex=1 -shell-escape -interaction=nonstopmode %O %S';

$out_dir = "build";
$aux_dur = "aux";
$pdf_mode = 1;

add_cus_dep('glo', 'gls', 0, 'glo2gls');
add_cus_dep('acn', 'acr', 0, 'acn2acr');
sub glo2gls {
    return system("makeindex -s '$_[0]'.ist -t '$_[0]'.glg -o '$_[0]'.gls '$_[0]'.glo");
}
sub acn2acr {
    return system("makeindex -s '$_[0]'.ist -t '$_[0]'.alg -o '$_[0]'.acr '$_[0]'.acn");
}
