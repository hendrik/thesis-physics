\chapter{Theory}
\label{ch:theory}
\section{Light-matter interactions}
As the name suggests the broad field of quantum optics uses optical means
to manipulate and control quantum objects. The interaction between
electromagnetic waves and matter is governed by the interaction between
the atoms electric dipole moment and the alternating electric field.
The force $\v{F}$ exerted on an atom in the presence of an electromagnetic
field is given by
\begin{equation}
    \v{F} = \underbrace{-\frac{\hbar\delta}{2}
        \frac{\nabla\sfrac{I}{I_{sat}}}
        {1+\sfrac{I}{I_{sat}}+\sfrac{4\delta^2}{\Gamma^2}}}_{\v{F}_{dipole}}
        + \underbrace{\hbar\v{k} \overbrace{\frac{\Gamma}{2}
        \frac{\sfrac{I}{I_{sat}}}{1 + \sfrac{I}{I_{sat}} +
        \sfrac{4\delta^2}{\Gamma^2}}}^{R_{scattering}}}_{\v{F}_{scattering}}
\end{equation}
with the detuning $\delta$, intensity $I$ and the wave vector $\v{k}$ of
the incident electromagnetic wave (derivation in~cite{foot2005atomic}).
The saturation intensity $I_{sat}$ is defined as 
\begin{equation}
    I_{sat} = \frac{\pi\hbar{} c}{3\lambda_0^3\tau}
\end{equation}
and $\Gamma$ is the natural linewidth of the atom. Derivation 

$\v{F}_{dipole}$ is referred to as the \emph{dipole force}. It is caused
by dispersion of the light by the atom. $\v{F}_{scattering}$ is referred
to as the \emph{scattering force}. It is due to absorption of spontaneous
emission of photons. The photons are emitted into a different mode
resulting in an average momentum transfer $\delta\v{p} = \hbar\v{k}$. The
rate $R_{scattering}$ at which this happens is referred to as the
\emph{scattering rate}.

For the later discussion the boundary case of strong detuning
($\abs{\delta} \gg \Gamma$) is especially relevant. This allows us to
neglect the scattering force and define the dipole potential
$\v{F}_{dipole} = - \nabla U_{dipole}$ by
\begin{equation}
    U_{dipole} = \frac{\hbar\Gamma}{8}\frac{\Gamma}{\delta}\frac{I}{I_{sat}}
    \label{eq:dipole_potential}
\end{equation}
The resulting potential depth is dependent on the intensity and its sign
on the detuning $\delta$. A positive (blue) detuning creates a repulsive
potential, a negative (red) detuning creates an attractive potential.

In the same regime the scattering rate can be approximated as
\begin{equation}
    R_{scattering} = \frac{\Gamma}{8} \frac{\Gamma^2}{\delta^2}
        \frac{I}{I_{sat}}.
\end{equation}
It is important to note that $U_{dipole} \propto \sfrac{I}{\delta}$ while
$R_{Scattering} \propto \sfrac{I}{\delta^2}$. This allows the constructions
of \emph{dipole traps} which capture the atoms at the center of a strong,
far detuned laser beam.

\section{Cooling atoms}
\subsection{Laser cooling}
Laser cooling uses the momentum transfer between photons and atoms to
decrease the atoms' kinetic energy, thus cooling them. The rate at which
photons are scattered and can transfer their momentum is dependent on the
light intensity. Assuming an intensity $I \gg I_{sat}$, the scattering
force simplifies to the maximum scatting force
\begin{equation}
    \v{F}_{max} = \hbar{}\v{k}\frac{\Gamma}{2}.
\end{equation}

While the momentum transfer due to one absorbed photon will always be in
the same direction, the spontaneously emitted photon is sent into a random
direction. Over the absorption and emission of many photons the momentum
transfer from the emitted photons will average to zero, thus resulting in
a net force in the direction of the incoming light wave.

At first glance this appears to be a great and simple design to decelerate
atoms. However, this scheme relies on the incoming beam being close to an
atomic resonance to scatter significant amounts of atoms. Due to the
Doppler shift the frequency experienced by the atom will change. This will
move the frequency away from resonance, reducing the accelerating force.
To cool atoms their kinetic energy needs to be reduced in all directions,
which requires a decelerating force from all directions.

The elegant solution to both problems is the so called \emph{optical
molasses}. It uses pairs of counter-propagating beams with the same
frequency $\omega$. For a stationary atom the forces exerted by the two
beams cancel each other out, leading to no net momentum transfer. Due to
the Doppler shift a moving atom will see one of the beams with increased
frequency (blue-detuned), the other with decreased frequency
(red-detuned).

By choosing the original frequency of the beams to be red-detuned to the
resonance frequency the beam against the movement direction will be moved
closer to resonance. This increases the absorption rate of the beam
against the movement direction. At the same time the absorption rate along
the direction of movement is reduced resulting in an effective force
against the direction of movement. By replicating this scheme in all three
directions of space the atoms are subject to a decelerating force
independent of their direction of movement, thus the atoms are being
cooled.

\subsection{Magneto-optical trapping}
\label{subsec:mot}
The optical molasses discussed in the previous section allows for
efficient cooling of atoms by providing a velocity-dependent dissipative
force, but does not add a restoring force necessary for trapping. A
\gls{MOT} combines the dissipative force with a trapping force generated
in a spatially-varying potential.

The trap uses the same light configuration of counter-propagating beams as
the optical molasses. Additionally a magnetic field with  zero field at
the center is created, usually with a pair of coils in anti-Helmholtz
configuration. The magnetic field shifts the energy levels of an atom due
to the \emph{Zeeman effect}. The resulting energy shift is given by
\begin{equation}
    E_{Zeeman} = g_{F}m_{F}\mu_{B}B_{z}(z)
\end{equation}
where $z$ is the quantization axis chosen by the magnetic field. The
shift leads to a spatially-dependent splitting of the energy states in the
$F = 1$ manifold. This moves the $m_{F} \neq 0$ states closer to resonance
with the red-detuned beams, which are used for the optical molasses as
depicted in figure~\ref{fig:mot}. The \gls{MOT} uses this splitting and
its dependence on the sign of $m_{F}$ to create a restoring force which
traps the atoms. This is achieved by polarizing the two
counter-propagating beams in opposite direction with respect to the
magnetic field. The $\sigma^+$ polarised beam can only address the
transactions into the $m_{F} = 1$ state, therefore increasing the detuning
on one side of the trap, while decreasing it on the other. The same is
true for the $\sigma^-$ polarised beam with opposite signs. By choosing
the polarisations correctly the atoms have a decreased detuning and
accordingly an increased absorption probability from the beam which pushes
them towards the center of the trap. As this is true for all directions of
space the atoms are trapped in the center of the \gls{MOT}.

\begin{figure}
    \centering
    \includegraphics[width=.45\textwidth]{images/MOT.pdf}
    \includegraphics[width=.45\textwidth]{images/drawingMOT.pdf}
    \caption{Schematic drawing of a 3D\,\gls{MOT} setup (left) and the
        corresponding energies (right).}
    \label{fig:mot}
\end{figure}

The optical cooling methods discussed so far rely on the absorption and
spontaneous reemission of photos where on average the spontaneous emission
will not transfer any momentum. This picture neglects the fluctuations of
this process which limit the achievable temperatures to the so called
\emph{Doppler cooling limit}. Some effects allow to cool below this
limit\footnote{This can be achieved through several effects, e.g. Sisyphus
cooling~\autocite{foot2005atomic}. As they can achieve temperatures below
the Doppler limit they are commonly called Sub-Doppler cooling effects.},
but fundamentally all optical cooling methods are limited to the
\emph{recoil limit}. This limit is a result of the energy kick gained by
emitting a photon while decaying from the excited state.

\subsection{Evaporative cooling}
To circumvent these fundamental limitations one needs to invoke methods
which do not rely on optical cooling. Evaporative cooling instead removes
the highest energetic atoms from the trap~\autocite{hess1986}. By doing so it
reduces the average kinetic energy of the remaining atoms, which leads to
reduced temperatures after thermalisation.

In optical dipole traps the hottest atoms can be removed by gradually
reducing the laser intensity, since the potential depth is proportional to
the intensity. In magnetic traps \gls{RF} which bring the atoms into an
un-trapped state can be used. The initial frequency is chosen to be
resonant together with the strong Zeeman shift on the outside of the trap
which can only be reached by the hottest atoms. Reducing the \gls{RF}
removes more and more atoms, gradually reducing the temperature in the
cloud.

This technique relies on the presence of a thermalisation mechanism to
reduce the temperature. Without thermalisation an out-of-equilibrium
state would be created. Thermalisation happens by inter-atomic scattering 
events. However Pauli blocking prevents scattering of fermions at low
temperatures, preventing effective thermalisation. \emph{Sympathetic
cooling} solves this problem by adding a second species with a strong
inter-species scattering cross section~\autocite{myatt1997}.

\section{Optical lattices}
We have already seen how the intensity dependent dipole potential
(equation~\ref{eq:dipole_potential}) can be used to trap atoms. By using
counter-propagating laser beams an optical standing wave can be created
which creates a fast alternating potential. In this potential the atoms
are attracted to the potential minima (blue detuned lattice beams) or the
potential maxima (red detuned lattice beams).

The resulting system of atoms moving in an optical potential can be seen
as a counterpart to electrons moving in the potential created by the ions
inside a crystal structure. The potential seen by the electrons can be
described as a $\sfrac{1}{r}$ potential generated by each ion, which can
be approximated to limited $\sfrac{1}{r}$ potentials due to screening
effects. Opposed to this complex description optical lattices can be
perfectly described by their $sin^2$ intensity oscillation, making their theoretical
description considerably simpler. This allows to solve the hamiltonian
analytically by using the Bloch theorem~\autocite{lewenstein2012}. This
resulting eigenenergies of this hamiltonian are in terms of a
quasimomentum $q$ and index by a band index $n$. The resulting band
structure for different lattice depths is plotted in
figure~\ref{fig:bandstructure}.

\begin{figure}
    \centering
    \includegraphics[width=.75\textwidth]{build/bandstructure.pdf}
    \caption{Band structure of a 1D optical lattice for lattice depths of
        $0$ (top left), $3$, $6$ and $20 E_{rec}$ (bottom right).
    }
    \label{fig:bandstructure}
\end{figure}

By increasing the laser intensity to form a deep lattice the distance
between the lowest band and the first excited band can become larger than
all other energies in the system. Therefore the influence of higher bands
can be neglected, which reduces the complexity of the system to that of
simpler many-body hamiltonians~\autocites{bloch2008}{esslinger2010}. The
relevant energy scale for weak lattices in this regime is given by the
recoil energy
\begin{equation}
    E_{rec} = \frac{\left( \hbar k \right)^2}{2m},
\end{equation}
where $k = \sfrac{2\pi}{\lambda}$ is the wave vector of the light forming
the lattice and $m$ is the atomic mass. The recoil energy makes a
convenient energy scale and is often used to compare energies.

\subsection{Hubbard model}
Under these conditions a \gls{BEC} in an optical lattice can be described
by the Bose-Hubbard hamiltonian
\begin{equation}
    \H = \underbrace{-J\sum_{\langle i,j \rangle}
    \hat{a}_i^{\dagger}\hat{a}_j}_{\text{hopping}} +
    \underbrace{\vphantom{\sum_{\langle}}\frac{U}{2} \sum_i
    \hat{n}_i(\hat{n}_i - 1)}_{\text{on-site interaction}} - \mu\hat{N},
    \label{eq:bose_hubbard_ham}
\end{equation}
where $\hat{a}_i^\dagger$ and $\hat{a}_i$ are the creation and
annihilation operators for a partical on site $i$. $\hat{n}_i =
\hat{a}_i^\dagger\hat{a}_i$ is the number operator, $\mu$ is the chemical
potential and $\hat{N}$ is the total particle number. $\sum_{\langle i,j
\rangle}$ summarises over neighbouring lattice sites.

This hamiltonian can be easily studied theoretically in the extreme cases.
In the case of $J \ll U$ the atoms are localised to the lattice sites,
which leads to dephasing of the condensate. For integer
fillings\footnote{Meaning the number of particles is an integer multiple
of the number of lattice sites.} a \emph{Mott insulator} is created as
movement between lattice sites is suppressed due to the strong on-site
interaction energy. The other case ($J \gg U$) leads to delocalisation of
the atoms throughout the lattice and creates a superfluid phase. The
transition from a Mott insulator to a superfluid phase is a quantum phase
transition. In contrast to classical phase transitions it is not driven by
temperature and can occur at zero Kelvin. 

Analogous to the Bose-Hubbard model one can write down the Fermi-Hubbard
model for fermions~\autocites{hubbard1963}{esslinger2010}
\begin{equation}
    \H = \underbrace{-J\sum_{\mathclap{\langle i,j \rangle,\sigma}}
    \hat{c}_{i,\sigma}^{\dagger}\hat{c}_{j,\sigma}}_{\text{hopping}} +
    \underbrace{\vphantom{\sum_{\langle}}U \sum_i
    \hat{n}_{i,\uparrow}\hat{n}_{i,\downarrow}}_{\text{on-site interaction}}
    \qquad \sigma \in \{\uparrow,\downarrow\},
    \label{eq:fermi_hubbard_ham}
\end{equation}
where $c_{i,\sigma}^\dagger$ is the creation operator of a fermion on site
$i$ in spin state $\sigma$. Fermi blocking limits the occupation of every
single site to a maximum of two particles\footnote{Assuming spin
$\sfrac{1}{2}$ particles.}. This system enters a Mott-insulator state at
an average filling of one particle per lattice site and $J \ll U$. If
$J \gg U$ the systems enters a metal state.

\section{Imaging methods}
Due to their low density clouds of ultracold atoms are relatively large,
which allows for observation of the atom cloud by optical means, i.e. by
taking an image. The two common methods are fluorescence imaging ---
observing the scattered photons --- and absorption imaging --- observing
the \enquote{shadow} created by the cloud. Both methods are destructive
measurement techniques as the desired interaction of the imaging light
with the particles heats the cloud and destroys the original state.

To gain statistical significance and to measure the influence of a varying
parameter many repetitions of the experimental sequence are done. This
requires the experimental setup to reliably create the same state using
the same experimental sequence.

A common measurement technique is the \emph{time-of-flight} measurement.
It creates images of the momentum distribution inside the cloud by mapping
momentum space into real space. At the beginning of the measurement the
confining potential is turned off immediately\footnote{The shut off has to
be fast enough to be non-adiabatically.}. Due to gravity the cloud starts
to fall as it expands due to the internal momentum distribution. An image
is taken after an expansion time --- the time of flight. Assuming a point
shaped cloud the resulting image is only dependent on the momentum of the
particles. For a finite cloud of non-interacting
particles\footnote{Interaction between the particles will influence the
expansion at the beginning, but can be neglected after a short expansion
period as the interaction probability drops with the density as the clouds
expands.} the resulting distribution is the convolution of the momentum
distribution and the original cloud shape. An expansion series can be
created by performing this measurement with varying time-of-flight
durations.

% vim: spell spelllang=en

