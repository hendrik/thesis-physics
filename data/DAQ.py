# -*- coding: utf-8 -*-
"""
@author Hendrik v. Raven

This file provides generic module implementations for cards based on
National Instruments NI-DAQmx library. They should be used in combination
with a NI TimingController.
"""

from nidaqmx import AnalogOutputTask, CounterOutputTask, DigitalOutputTask
from collections import namedtuple
from qcontrol.server import timing
from qcontrol.tools.units import ns, ms, s, V, Hz, kHz
from qcontrol.tools.quantizer import dictquantizer, linear_quantizer
from math import ceil
from qcontrol.server.drivers.NationalInstruments import RTSI

import numpy as np

class Module(timing.Module):
    def __init__(self, name, parent, *args, **kwargs):
        super(Module,self).__init__(name, parent = parent, *args, **kwargs)

    @property
    def master_card(self):
        return self.parent.master_card

    '''
    self.channels contains physical channels as well as aliases.
    This just returns the physical channels
    '''
    def phys_channels(self):
        return {name: chan for name, chan in self.channels.iteritems()
                if chan.parent == self}


class SubModule(Module):
    def __init__(self, name, parent, *args, **kwargs):
        super(SubModule, self).__init__(name, parent = parent, *args,
                **kwargs)

    @property
    def device_name(self):
        return self.parent.device_name

class Card(Module):
    """
    A card represents a physical card which is plugged into the computer.
    It contains at least one outout or input module as submodule.
    """
    def __init__(self, name, device_name, parent, counter = None,
            clocks = [], ao = None, ai = None, do = None,
            mastercard = None, reference_clock = None):
        super(Card, self).__init__(name, parent = parent)
        self.device_name = device_name
        self._counter = counter
        self._stopCallback = None
        self._clocks = clocks
        self._ao = ao
        self._ai = ai
        self._do = do
        self._refclk = reference_clock

        if mastercard:
            self.master_clock_line = RTSI.requestLane()
            if not self._counter:
                raise Exception("A master card requires counter.")
            self._initCounter()
            self.Counter.initMasterClock(output = self.master_clock_line,
                                         **mastercard)
            parent.master_card = self
            self._initSubmodules()

        else:
            self._initCounter()
            self._initSubmodules()

    def compile(self, sequence):
        data = {}
        for name,mod in self.modules.iteritems():
            data[name] = mod.compile(sequence)
        return data

    def setNewData(self, data):
        for name,mod in self.modules.iteritems():
            mod.setNewData(data[name])

    def start(self):
        for name,mod in self.modules.iteritems():
            # the driver code is build arount the assumption that everything
            # is waiting for the master counter to be started. Make sure that
            # actually is the case
            if name == 'Counter':
                continue
            mod.start()
        if 'Counter' in self.modules:
            self.Counter.start()

    def stop(self):
        if 'Counter' in self.modules:
            self.Counter.stop()
        for name,mod in self.modules.iteritems():
            if name == 'Counter':
                continue
            mod.stop()

    def isMasterCard(self):
        return self.parent.master_card == self

    def getMasterClock(self):
        return self.parent.master_card.Counter.MasterClock.clock

    def _initCounter(self):
        if self._counter:
            Counter('Counter', self, self._counter, self._clocks, self._refclk)

    def _initSubmodules(self):
        if self._ao:
            AnalogOutput('AO', self, **(self._ao))
        if self._do:
            DigitalOutput('DO', self, **(self._do))

    def requestTimer(self, freq, outs):
        return self.Counter.getClock(freq, outs)

    def setStopCallback(self, fun):
        if not 'Counter' in self.modules:
            raise Exception('Can\'t set stop callback without a counter submodule.')
        else:
            self.Counter.setStopCallback(fun)

class TaskModule(SubModule):
    _task = None

    def __del__(self):
        if self._task:
            del self._task

    def isIdle(self):
        if not self._task:
            return True
        return self._task.is_done()

    def start(self):
        if self._task:
            self._task.start()

    def stop(self):
        if self._task:
            self._task.stop()

# class which adds an extra counter task
class CtrTaskModule(TaskModule):
    _ctr_task = None

    def __del__(self):
        super(CtrTaskModule, self).__del__()
        if self._ctr_task:
            del self._ctr_task

    def start(self):
        super(CtrTaskModule, self).start()
        if self._ctr_task:
            self._ctr_task.start()

    def stop(self):
        super(CtrTaskModule, self).stop()
        if self._ctr_task:
            self._ctr_task.stop()

# class which adds state tracking of the output channels
class OutputStateModule(CtrTaskModule):
    def __init__(self, name, channels, **kwargs):
        super(OutputStateModule, self).__init__(name, channels=channels, **kwargs)
        self._current_data = None
        self._state = dict([(chan, chan.default_value) for chan in channels])

    def get_output(self, channel):
        state = self._state[channel]
        if state != None:
            return state
        else:
            raise Exception('State is currently unknown')

    def set_output(self, channel, value):
        if not self.parent.parent.isIdle():
            raise Exception('Can only set output values if the system is idle')

        # we need to create another task to set the value. To make sure the
        # channel we want to access is not reserved we move the main task to
        # the verified state which is in front of the reserve state
        # see 'Task State Model' in the DAQmx documentation
        if self._task:
            del self._task
            self._task = None
        if self._ctr_task:
            del self._ctr_task

        # this needs to be implemented in the classes inheriting from this
        task = self._set_output(channel, value)

        task.wait_until_done()

        # delete task to free resources
        del task

        # move the state back to the commited state
        if self._current_data:
            self.setNewData(self._current_data)

        # update the state information
        self._state[channel] = value

    def start(self):
        # during the sequence we can't say anything about the current value
        self._state = dict([(chan,None) for chan in self.phys_channels()])
        super(OutputStateModule, self).start()

    def stop(self):
        # now the values are the final values of the sequence
        self._state = dict([(chan, chan.default_value)
                            for chan in self.phys_channels().itervalues()])
        super(OutputStateModule, self).stop()

class AnalogOutput(OutputStateModule):
    def __init__(self, name, parent, resolution, channels,
           valueunit = V, **channelargs):
        channels = [AOutChannel(n, valueunit=valueunit, **channelargs) for n in channels]
        super(AnalogOutput,self).__init__(name, channels = channels,
                parent=parent, valueunit=valueunit)
        self._timer, self._resolution = self.master_card.requestTimer(resolution,
                RTSI.allLanes())
        self._timequantizer = linear_quantizer(offset = 0*s,
                scale = 1./self._resolution)
        self._ctr = '/'.join(['', self.device_name,
                                  self.parent.Counter.requestCounter('AO Gate')])
        self._pfi = '/'.join(['', self.device_name, 'pfi1'])

    def _set_output(self, channel, value):
        phys_channel = '/'.join(['', self.device_name, channel.name])
        task = AnalogOutputTask()
        task.create_voltage_channel(phys_channel,
                                    min_val=-10,
                                    max_val=10)
        # we just need some clock, so we use the OnboardClock
        task.configure_timing_sample_clock(source='OnboardClock',
                                           rate=self._resolution.rescale(Hz),
                                           sample_mode='finite',
                                           samples_per_channel=2)
        v = value.rescale(self.valueunit)
        # write requires at least two values, so we just set them twice
        task.write(data=[v,v], auto_start=True)
        return task

    def compile(self, sequence):
        # first get all necessary timestamps
        times = [[0]]
        chans = list(self.phys_channels().itervalues())
        for chan in chans:
            entries = sequence.flat_table.get(chan.full_name, [])
            if entries == []:
                continue

            times.append(np.array(entries.time * self._resolution.rescale(Hz),
                                  dtype=np.uint32))

        # make sure we have timestamps for the begin and the end of the sequence
        times.append([0, sequence.duration * self._resolution + 1])

        # unique sorts and removes duplicates
        times = np.unique(np.concatenate(times).astype(np.uint32))

        data = np.empty((len(chans),len(times)), dtype=np.float)
        for i, chan in enumerate(chans):
            entries = sequence.flat_table.get(chan.full_name, [])
            # unchanged channels are just set to their default_value all the time
            if entries == []:
                data[i][:] = chan.default_value.rescale(self.valueunit)
                continue

            t = (entries.time * self._resolution.rescale(Hz)).astype(np.uint32)
            v = entries.value

            # make sure we have a value for the first update and add the end
            # value. We add the maximal value to make sure that searchsorted
            # will end with the last index
            t = np.insert(t, [0, len(t)], [0, np.iinfo(np.uint32).max])
            v = np.insert(v, 0, chan.default_value.rescale(self.valueunit))

            # searchsorted returns the indices at which the timestamps can be
            # found
            idx = np.searchsorted(times, t)

            data[i] = np.repeat(v, np.diff(idx))
            # reset channel to default at the end of the sequence
            data[i][-1] = chan.default_value.rescale(self.valueunit)

        ## convert timestamps to high and low ticks (low = active)
        # find block boundaries (marking end of each block, except last)
        idx = ((times[1:] - times[:-1]) != 1)
        # convert boundaries into start & end of blocks
        boundaries = np.concatenate((times[idx]+1, times[1:][idx],
                                     [times[0], times[-1]+1, times[-1]+3]))
        boundaries.sort()
        ticks = np.diff(boundaries)

        # scale ticks based on the frequency difference of our clock vs. the
        # master clock which the gate is using.
        multiplier = int(self.parent.getMasterClock().real_frequency / self._resolution)

        # we need at least 2 ticks per group, otherwise the driver complains
        if len(ticks) == 2:
            ticks = np.append(ticks, [multiplier,multiplier])

        ticks *= multiplier
        # decrease first low duration to switch in between clock cycles
        ticks[0] -= multiplier // 2

        high_ticks = ticks[1::2]
        low_ticks = ticks[::2]

        return (chans, data, (low_ticks, high_ticks))

    def setNewData(self, input):
        if self._task:
            self._task.stop()
            del self._task
        if self._ctr_task:
            self._ctr_task.stop()
            del self._ctr_task
        self._task = None
        self._ctr_task = None

        # store current data, we might need it later if we set channels manually
        self._current_data = input

        chans,data,(low_ticks, high_ticks) = input

        # we create a new task
        self._task = AnalogOutputTask()
        for channel in chans:
            name = '/'.join(['', self.device_name, channel.name])
            self._task.create_voltage_channel(name,
                    min_val=-10.0, max_val=10.0, units = 'volts')

        # set the duration of a cycle
        self._task.configure_timing_sample_clock(
             source = '/' + self.device_name + '/' + self._timer,
             sample_mode = 'finite',
             samples_per_channel = len(data[0]))
        self._task.set_pause_trigger('digital_level')
        self._task.set_pause_trigger_when('high')
        self._task.set_pause_trigger_source(self._pfi)
        self._task.write(data, auto_start = False,
                         layout = 'group_by_channel')

        master = self.parent.getMasterClock().outputs[0]
        self._ctr_task = CounterOutputTask()
        self._ctr_task.create_channel_ticks(self._ctr, source=master,
                                            low_ticks=2, high_ticks=2)
        self._ctr_task.configure_trigger_disable_start()
        self._ctr_task.set_terminal_pulse(self._ctr, self._pfi)
        self._ctr_task.configure_timing_implicit(sample_mode='finite',
                                                 samples_per_channel=len(low_ticks))
        self._ctr_task.write_ticks(high_ticks, low_ticks, auto_start=False)

class AOutChannel(timing.Channel):
    def __init__(self, name, default_value = 0*V, minimum = -10*V,
            maximum = 10*V, offset = 0*V, precision = 16, valueunit = V):
        quantizer = linear_quantizer(minimum = minimum, maximum = maximum,
               offset = offset,
               scale = (maximum - minimum)/(2**precision))
        super(AOutChannel, self).__init__(name = name,
                valueunit = valueunit, quantizer = quantizer,
                hw_hint = name, default_value = default_value,
                description = 'analog output %s'%name)

    # overwrite the default to pass the channel instead of the hw_hint
    def get(self):
        return self.parent.get_output(self)

    def set(self, value):
        self.parent.set_output(self, value)

class DigitalOutput(OutputStateModule):
    def __init__(self, name, parent, resolution, port, number_of_channels,
                 default_values = {}):
        self._default_value = np.uint32(0)
        channels = [DOutChannel(i, default_value=default_values.get(i, 0))
                    for i in range(number_of_channels)]
        super(DigitalOutput,self).__init__(name, channels = channels,
                parent = parent, valueunit = None)
        self._port = port
        self._timer, self._resolution = self.master_card.requestTimer(resolution,
                                                                      RTSI.allLanes())
        self._timequantizer = linear_quantizer(offset = 0*s,
                scale = 1./self._resolution)
        self._ctr = '/'.join(['', self.device_name,
                                  self.parent.Counter.requestCounter('DIO Gate')])
        self._pfi = '/'.join(['', self.device_name, 'pfi0'])

    def _set_output(self, channel, value):
        phys_channel = '/'.join(['', self.device_name, self._port,
                                 'line%d'%channel.hw_hint])
        task = DigitalOutputTask()
        task.create_channel(phys_channel)
        # we just need some clock, so we use the OnboardClock
        task.configure_timing_sample_clock(source='OnboardClock',
                                           rate=self._resolution.rescale(Hz),
                                           sample_mode='finite',
                                           samples_per_channel=2)
        # write requires at least two values, so we just set them twice
        task.write(data=[value, value], auto_start=True)
        return task

    def compile(self, sequence):
        # compilation is a three step process. First we collect all the
        # timestamps and make sure we fullfill all the constraints.
        # Using the updated list we then generate the list of values for these
        # timestamps. In the last step we convert the timestamps in ticks to
        # wait between the updates

        ## Timestamp generation
        # We can't generate single pulses, we always have to generate at least
        # two. On the same scale we need a pause of at least to ticks between
        # pulses. We add the necessary pulses to fullfill all requirements here

        # collect all timestamps and convert them to ticks
        otimes = []
        for chan in self.phys_channels().itervalues():
            entries =  sequence.flat_table.get(chan.full_name, [])
            if entries == []:
                continue

            times = np.array(entries.time * self._resolution.rescale(Hz),
                             dtype = np.uint32)
            otimes.append(times)
            # add the next value to make sure that each pulse is at least 2
            # timestamps long. We remove duplicates later
            # TODO this adds unnecessary timestamps at some points
            otimes.append(times + 1)

        # we need to make sure that the sequence start and end get timestamps
        end = sequence.duration * self._resolution
        otimes.append([0, 1, end, end+1])

        # np.unique sorts and removes duplicates
        times = np.unique(np.concatenate(otimes).astype(np.uint32))

        # we need to check for gaps of width one (delta to next entry == 2).
        # If they exist we add a new timestamp at that position.
        # get the inidices at which this occurs. These point to the value in
        # front of the gap
        idx = np.nonzero((times[1:] - times[:-1]) == 2)[0]
        if len(idx) > 0:
            # get the timestamps at those positions
            vals = np.take(times, idx)
            # increment to get the missing timestamp
            vals += 1

            # insert requires indices pointing behind the gap
            idx += 1
            times = np.insert(times, idx, vals)

        # Compute the values
        data = np.empty((len(self.phys_channels()),len(times)), dtype = np.uint8)
        for chan in self.phys_channels().itervalues():
            entries =  sequence.flat_table.get(chan.full_name, [])
            # unchanged channels are just set to their default_value all the time
            if entries == []:
                data[chan.hw_hint][:] = chan.default_value
                continue

            t = (entries.time * self._resolution.asNumber()).astype(np.uint32)
            v = entries.value

            # make sure we have a value for the first update and add the end
            # value. We add the maximal value to make sure that searchsorted
            # will end with the last index
            t = np.insert(t, [0, len(t)], [0, np.iinfo(np.uint32).max])
            v = np.insert(v, 0, chan.default_value)

            # searchsorted returns the indices at which the timestamps can be
            # found
            idx = np.searchsorted(times, t)

            data[chan.hw_hint] = np.repeat(v, np.diff(idx))
            # reset channel to default at the end of the sequence
            data[chan.hw_hint][-1] = chan.default_value

        ## convert timestamps to high and low ticks (low = active)
        # find block boundaries (marking end of each block, except last)
        idx = ((times[1:] - times[:-1]) != 1)
        # convert boundaries into start & end of blocks
        boundaries = np.concatenate((times[idx]+1, times[1:][idx],
                                     [times[0], times[-1]+1, times[-1]+3]))
        boundaries.sort()
        ticks = np.diff(boundaries)

        # we need at least 2 ticks per group, otherwise the driver complains
        if len(ticks) == 2:
            ticks = np.append(ticks, [2,2])

        high_ticks = ticks[1::2]
        low_ticks = ticks[::2]

        return (data,(low_ticks,high_ticks))

    def setNewData(self, input):
        if self._task:
            self._task.stop()
            del self._task
        if self._ctr_task:
            self._ctr_task.stop()
            del self._ctr_task
        self._task = None
        self._ctr_task = None

        self._current_data = input
        data,(low_ticks,high_ticks) = input

        self._task = DigitalOutputTask()
        self._task.create_channel('/'.join(['', self.device_name, self._port,
                                           'line0:%i'%(len(self.phys_channels())-1)]),
                                  grouping = 'per_line')
        self._task.configure_timing_sample_clock(source = self._timer,
                                                 sample_mode = 'finite',
                                                 samples_per_channel = len(data[0]))
        self._task.set_pause_trigger('digital_level')
        self._task.set_pause_trigger_when('high')
        self._task.set_pause_trigger_source(self._pfi)
        self._task.write(data, auto_start = False,
                         layout = 'group_by_channel')

        rtsi = '/'.join(['', self.device_name, self._timer])
        self._ctr_task = CounterOutputTask()
        self._ctr_task.create_channel_ticks(self._ctr, source=rtsi,
                                            low_ticks=2, high_ticks=2)
        self._ctr_task.configure_trigger_disable_start()
        self._ctr_task.set_terminal_pulse(self._ctr, self._pfi)
        self._ctr_task.configure_timing_implicit(sample_mode='finite',
                                                 samples_per_channel=len(low_ticks))
        self._ctr_task.write_ticks(high_ticks, low_ticks, auto_start=False)

class DOutChannel(timing.Channel):
    def __init__(self, number, default_value=0):
        super(DOutChannel,self).__init__(name = 'line%i'%number,
                valueunit = None, default_value = default_value,
                quantizer = dictquantizer({'low':0, 'high': 1}),
                hw_hint = number)

    def set(self, value):
        self.parent.set_output(self, self.mapFunction(0, value)[1])

    def get(self):
        return self.parent.get_output(self)

    def mapFunction(self, times, values):
        # TODO
        # this is an ugly fix which is here because the GUI sends the strings
        # high or low if you want to set a value. We should get the GUI to send
        # 0 or 1 instead.
        if isinstance(values, basestring):
            return times, {'low':0, 'high': 1}[values]
        else:
            return times, values

class Counter(TaskModule):
    ClockConfig = namedtuple('ClockConfig',
            'source, frequency, real_frequency, outputs')

    def __init__(self, name, parent, counters, clocks, refclk):
        super(Counter, self).__init__(name, parent = parent,
                timeunit = ns)
        self._counters = dict([(c, None) for c in counters])
        self._clocks = [self.ClockConfig(None, f, f, [o]) for o,f in
                clocks.iteritems()]

    def initMasterClock(self, **kwargs):
        ctr = self.requestCounter('master')
        Counter.MasterModule(parent=self, ctr=ctr, **kwargs)

    def requestCounter(self, usage):
        """
        returns the name of an not yet used counter. Throws an exception if
        no further counter is available
        """
        for (k,v) in self._counters.iteritems():
            if v == None:
                self._counters[k] = usage
                return k

        raise Exception('New counter was requested, but none available')

    def getClock(self, frequency, outputs):
        """
        this asks the card for a clock signal with the requested
        frequency on one of the requested outputs.

        frequency: the desired output frequency

        outputs: a list of possible outputs, sorted by priority (highest
            first)

        return: (output, real frequency)
            output prints the used output, real frequency shows the exact
            frequency the timer is running at
        """

        # first check if there is already a timer running matching the
        # request
        for c in self._clocks:
            if c.frequency == frequency:
                # skip all non counter based clocks
                if not c.source:
                    continue
                return c.outputs[0], c.real_frequency

        # next check if we have a timer with the correct frequency which
        # we can route to the given output
        for c in self._clocks:
            if c.frequency == frequency:
                # skip all non counter based clocks
                if not c.source:
                    continue
                output = RTSI.requestLane(outputs)
                self._task.set_terminal_pulse(c.source, output)
                c.outputs.append(output)
                return output, c.real_frequency

        # allocate a new counter for the given frequency
        ctr = self.requestCounter('temp')
        output = RTSI.requestLane(outputs)
        clock = self._initClock(ctr, frequency, output)
        self._counters[ctr] = clock
        self._clocks.append(clock)
        return clock.outputs[0], clock.real_frequency

    def _initClock(self, ctr, freq, output):
        # all counters need to use the master clock
        refclk = self.parent.getMasterClock()

        ctr = '/'.join(['', self.device_name, ctr])
        ref = '/'.join(['', self.device_name, refclk.outputs[0]])
        out = '/'.join(['', self.device_name, output])

        high_ticks = 2 # this has to be at least two, otherwise this generates errors
        low_ticks = int(ceil(refclk.real_frequency/freq)) - high_ticks
        real_freq = refclk.real_frequency / (low_ticks + high_ticks)

        # initialize clock task on first use
        if self._task == None:
            self._task = CounterOutputTask()

        self._task.create_channel_ticks(ctr, source = ref,
                low_ticks =  low_ticks, high_ticks = high_ticks)
        self._task.set_terminal_pulse(ctr, out)
        self._task.configure_timing_implicit()

        return self.ClockConfig(ctr, freq, real_freq, [output])

    def setStopCallback(self, cbk):
        if 'StopCallback' in self.modules:
            self.StopCallback.setStopCallback(cbk)
        else:
            ctr = self.requestCounter('Callback')
            Counter.CallbackModule(parent=self, ctr=ctr, callback=cbk)

    def compile(self, sequence):
        data = {}
        for name,mod in self.modules.iteritems():
            data[name] = mod.compile(sequence)
        return data

    def setNewData(self, data):
        for name,mod in self.modules.iteritems():
            mod.setNewData(data[name])

    def start(self):
        if self._task:
            self._task.start()

        for name,mod in self.modules.iteritems():
            # the driver code is build arount the assumption that everything
            # is waiting for the master counter to be started. Make sure that
            # actually is the case
            if name == 'MasterClock':
                continue
            mod.start()
        if 'MasterClock' in self.modules:
            self.MasterClock.start()

    def stop(self):
        if self._task:
            self._task.stop()
        for module in self.modules.itervalues():
            module.stop()

    class MasterModule(TaskModule):
        def __init__(self, parent, ctr, output, frequency):
            super(Counter.MasterModule, self).__init__('MasterClock',
                                                       parent=parent)

            self.clock = self.parent.ClockConfig(ctr, frequency, frequency,
                                                  [output])
            self.parent._clocks.append(self.clock)

            self._frequency = frequency

            ctr    = '/'.join(['', self.device_name, ctr])
            output = '/'.join(['', self.device_name, output])

            self._task = CounterOutputTask()
            self._task.create_channel_frequency(ctr, freq=frequency.rescale(Hz),
                                                units='hertz', duty_cycle=0.5)
            self._task.set_terminal_pulse(ctr, output)
            self._task.configure_trigger_disable_start()
            # pause trigger does only work with continuous timing
            self._task.configure_timing_implicit(sample_mode='continuous')

        def compile(self, sequence):
            return None

        def setNewData(self, data):
            pass

        def setPauseTrigger(self, source, pause_on='low'):
            if source == None:
                # disable pause trigger
                self._task.set_pause_trigger(None)
            else:
                source = '/'.join(['', self.device_name, source])
                self._task.set_pause_trigger('digital_level')
                self._task.set_pause_trigger_when(pause_on)
                self._task.set_pause_trigger_source(source)

    class CallbackModule(TaskModule):
        def __init__(self, parent, ctr, callback, freq = 1*kHz):
            super(Counter.CallbackModule, self).__init__('StopCallback',
                                                         parent=parent)
            self._freq = freq

            refclk = self.parent.parent.getMasterClock()
            ticks = int(refclk.real_frequency / freq)

            self._task = CounterOutputTask()

            ctr = '/'.join(['', self.device_name, ctr])
            ref = '/'.join(['', self.device_name, refclk.outputs[0]])
            self._task.create_channel_ticks(counter=ctr, source=ref,
                                            low_ticks=2, high_ticks=(ticks-2))

            self.setStopCallback(callback)

        def setStopCallback(self, fun):
            # TODO make this local
            global _stopCallback
            _stopCallback = fun
            self._task.register_done_event(self._doneEventHandler)

        # this function gets called by the DAQmx library as callback. The function
        # signature is fixed by the library at it has to return 0
        def _doneEventHandler(task, status, cb_data = None):
            print('doneEventHandler called')
            if _stopCallback:
                _stopCallback()
            return 0

        def compile(self, sequence):
            return int(ceil((sequence.duration + 1*ms) * self._freq))

        def setNewData(self, data):
            self._task.configure_timing_implicit(sample_mode='finite',
                                                 samples_per_channel=data)